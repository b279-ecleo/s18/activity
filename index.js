console.log("Hello World")

function addNum(num1, num2) {
	let sum = num1 + num2;
  	console.log	("Displayed Sum of " + num1 + " and " + num2);
  	console.log(sum);
}
addNum (5,15);


function subNum(num1, num2) {
	let difference = num1 - num2;
  	console.log	("Displayed difference of " + num1 + " and " + num2);
  	console.log(difference);
}
subNum (20,5);

function multiplyNum(num1, num2) {
	let product = num1 * num2;
  	console.log	("The Product of " + num1 + " and " + num2);
  	console.log(product);
}
multiplyNum (50,10);

function divideNum(num1, num2) {
	let quotient = num1 / num2;
  	console.log	("The quotient of " + num1 + " and " + num2);
  	console.log(quotient);
}
divideNum (50,10);

function getCircleArea(radius) {
  const pi = 3.14159;
  let circleArea = pi * radius ** 2;
  console.log("The result of getting the area of a circle with " + radius + " radius is: ");
  console.log(circleArea);
}

getCircleArea(15);


function getAverage(num1, num2, num3, num4) {
  let total = num1 + num2 + num3 + num4;
  let averageVar = total / 4;
  console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", and " + num4);
  return averageVar;
}

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);

function checkIfPassed(score, totalScore) {
  let percentage = (score / totalScore) * 100;
  let isPassed = percentage > 75;
  console.log("Is " + score + "/" + totalScore + " a passing score?");
  return isPassed;
}


let isPassingScore = checkIfPassed(38,50);
console.log(isPassingScore);